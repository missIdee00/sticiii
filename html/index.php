<!DOCTYPE html>
<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  </script>
  <script src="../javascript/script.js"></script>

  <style>
    .modal-header,
    h4,
    .close {
      background-color: #5cb85c;
      color: white !important;
      text-align: center;
      font-size: 30px;
    }

    .modal-footer {
      background-color: #f9f9f9;
    }
  </style>
</head>

<body>

  <div class="container">
    <h2>Liste de Produit</h2>
    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-success btn-lg" id="myBtn" data-toggle="modal" data-target="#myModal">Enregistrer un produit</button>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style="padding:35px 50px;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4><span class="glyphicon glyphicon-lock"></span> Enregistrement</h4>
          </div>
          <div class="modal-body" style="padding:40px 50px;">
            <div id="response">
              <form role="form" id="form" action="../php/saveProduit.php" method="POST" enctype="multipart/form-data">
                <input type="hidden" class="form-control" id="id" value="" name="id">
                <div class="form-group">
                  <input type='file' name='userImage' multiple />
                </div>
                <div class="form-group">
                  <label for="article"> Article</label>
                  <input type="text" class="form-control" id="article" name="article">
                </div>
                <div class="form-group">
                  <label for="nature"> Nature</label>
                  <input type="text" class="form-control" id="nature" name="nature">
                </div>
                <div class="form-group">
                  <label for="quantity_initiale">Quantity initiale</label><input type="text" id="quantity_initiale" name="quantity_initiale" class="form-control">
                </div>
                <div class="form-group">
                  <label for="quantity_restante">Quantity finale</label>
                  <input type="text" id="quantity_restante" name="quantity_restante" class="form-control">
                </div>
                <div class="form-group">
                  <label for="prix">Prix</label><input type="text" id="prix" name="prix" class="form-control">
                </div>
                <div class="form-group">
                  <label for="localisation">Localisation</label><input type="text" id="localisation" name="localisation" class="form-control">
                </div>
                <div class="form-group">
                  <label for="poids_vol">Poids/Volume</label><input type="text" id="poids_vol" name="poids_vol" class="form-control">


                </div>
                <button type="submit" class="btn btn-success btn-block" name="Enregistrer" id="load"><span class="glyphicon glyphicon-off"></span> Enregistrer</button>
              </form>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Annuler</button>
        </div>
      </div>
    </div>
  </div>
  <table class="table ">
    <thead class="thead">
      <tr>
        <th scope="col">ID</th>
        <th scope="col">Article</th>
        <th scope="col">Photo</th>
        <th scope="col">Nature</th>
        <th scope="col">Quantité Initiale</th>
        <th scope="col">Quantité Restante </th>
        <th scope="col">Prix </th>
        <th scope="col">Localisation</th>
        <th scope="col">Poids/Volume </th>
        <th scope="col">Modification</th>
        <th scope="col">Suppression</th>
      </tr>
    </thead>
    <tbody>
      <?php
      include("../php/connection.php");
      $query = "SELECT *FROM produit order by id desc ";
      $donn = $conn->query($query)->fetchAll(PDO::FETCH_ASSOC);
      foreach ($donn as  $value) :; ?>

    <tbody>
      <tr>
        <th scope="row"><?php echo $value['id']; ?></th>
        <td> <?php echo $value['article']; ?></td>
        <td><img src="..\C:\Users\Aidambow\Desktop\DCIM - Copie\Camera<? echo $value['images']; ?>"></td>
        <td> <?php echo $value['nature']; ?></td>
        <td> <?php echo $value['quantity_initiale']; ?></td>
        <td> <?php echo $value['quantity_restante']; ?></td>
        <td> <?php echo $value['prix']; ?></td>
        <td> <?php echo $value['localisation']; ?></td>
        <td> <?php echo $value['poids_vol']; ?></td>

        <td><a href=" ../php/formModifier.php?num=<?php echo $value['id'] ?>" class="btn btn-success"><span class='glyphicon glyphicon-edit'></span>modifier</a></td>
        <td><a href="../php/formSupprimer.php?num=<?php echo $value['id'] ?>" class="btn btn-danger"><span class='glyphicon glyphicon-remove'></span>supprimer</a></td>

      </tr>
    <?php endforeach; ?>
    </tbody>
  </table>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

</body>

</html>