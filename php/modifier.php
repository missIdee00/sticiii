<?php
include('connection.php');
$query = $conn->prepare("UPDATE produit set article=:article,nature=:nature,quantity_initiale=:quantity_initiale,quantity_restante=:quantity_restante,prix=:prix,localisation=:localisation,poids_vol=:poids_vol where id=:id limit 1");
$query->bindValue(':id', $_POST['id'], PDO::PARAM_INT);
$query->bindValue(':article', $_POST['article'], PDO::PARAM_STR);
$query->bindValue(':nature', $_POST['nature'], PDO::PARAM_STR);
$query->bindValue(':quantity_initiale', $_POST['quantity_initiale'], PDO::PARAM_STR);
$query->bindValue(':quantity_restante', $_POST['quantity_restante'], PDO::PARAM_STR);
$query->bindValue(':prix', $_POST['prix'], PDO::PARAM_STR);
$query->bindValue(':localisation', $_POST['localisation'], PDO::PARAM_STR);
$query->bindValue(':poids_vol', $_POST['poids_vol'], PDO::PARAM_STR);
$executeOk=$query->execute();
?>
