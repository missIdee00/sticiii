<?php
include('connection.php');
$update = $conn->prepare("SELECT *from produit where id = :num");
$update->bindValue(':num', $_GET['num'], PDO::PARAM_INT);
$executeOk = $update->execute();
$id = $update->fetch();
?>

<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    </script>
    <script src="../javascript/script.js"></script>

    <style>
        .modal-header,
        h4,
        .close {
            background-color: #5cb85c;
            color: white !important;
            text-align: center;
            font-size: 30px;
        }

        .modal-footer {
            background-color: #f9f9f9;
        }
    </style>
</head>

<body>
    <form role="form" id="form" action="modifier.php" method="POST">
        <input type="hidden" class="form-control" id="id" value="<?= $id['id']; ?>" name="id">
        <div class="form-group">
            <label for="article"> Article</label>
            <input type="text" class="form-control" id="article" name="article" value="<?= $id['article']; ?>">
        </div>
        <div class="form-group">
            <label for="nature"> Nature</label>
            <input type="text" class="form-control" id="nature" name="nature" value="<?= $id['nature']; ?>">
        </div>
        <div class="form-group">
            <label for="quantity_initiale">Quantity initiale</label><input type="text" id="quantity_initiale" name="quantity_initiale" class="form-control" value="<?= $id['quantity_initiale']; ?>">
        </div>
        <div class="form-group">
            <label for="quantity_restante">Quantity finale</label>
            <input type="text" id="quantity_restante" name="quantity_restante" class="form-control" value="<?= $id['quantity_restante']; ?>">
        </div>
        <div class="form-group">
            <label for="prix">Prix</label><input type="text" id="prix" name="prix" class="form-control" value="<?= $id['prix']; ?>">
        </div>
        <div class="form-group">
            <label for="localisation">Localisation</label><input type="text" id="localisation" name="localisation" class="form-control" value="<?= $id['localisation']; ?>">
        </div>
        <div class="form-group">
            <label for="poids_vol">Poids/Volume</label><input type="text" id="poids_vol" name="poids_vol" class="form-control" value="<?= $id['poids_vol']; ?>">
        </div>
        <button type="submit" class="btn btn-success btn-block" name="Enregistrer" id="load"><span class="glyphicon glyphicon-off"></span> Enregistrer</button>
    </form>
    </div>
    </div>
    </div>

    <button type="submit" class="btn btn-danger btn-default pull-left"><span class="glyphicon glyphicon-remove"></span> Annuler</button>
    </div>
    </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

</body>

</html>