var pdtId = 0;
var data = [];
var formData;
//fonction qui permet d'effacer le contenu du tablaeu à chak fois kon actualise
clear = function (f) {
    f.article.value = '';
    f.nature.value = '';
    f.qté_ini.value = '';
    f.qté_res.value = '';
    f.prix.value = '';
    f.localisation.value = '';
    f.poids_vol.value = '';

}
//fonction qui permet de creer une colonne
addCellInRow = function (line, data) {
    var cell = line.insertCell();;//creer une colonne
    var text = document.createTextNode(data);//pour mettre du texte
    cell.appendChild(text);
}
//fonction qui permet d'ajouter des bouttons d'acces qui servent à modifier les champs de la ligne correspondante 
addCellButtonInRow = function (line, value, dataId, cssClass, dataTarget) {
    var cell = line.insertCell();
    var button = document.createElement("button");
    button.innerHTML = value;
    button.setAttribute('data', dataId);
    button.setAttribute('class', cssClass);
    button.setAttribute('data-Target', dataTarget)
    button.addEventListener("click", function () {
        console.log(button);
        if (this.getAttribute('data-Target') == 'edit') {
            console.log('edit button');
            for (let i = 0; i < data.length; i++) {
                if (data[i].id == this.getAttribute('data')) {
                    formData.article.value = data[i].article;
                    formData.nature.value = data[i].nature;
                    formData.qté_ini.value = data[i].qté_ini;
                    formData.qté_res.value = data[i].qté_res;
                    formData.prix.value = data[i].prix;
                    formData.localisation.value = data[i].localisation;
                    formData.poids_vol.value = data[i].poids_vol;
                    

                }
                break;

            }
        }
        else {
            //console.log('edit button');
        }
    });
    cell.appendChild(button);

}
//fonction qui permet de valider les valeurs entrer dans le formulaires ainsi que de stockers ses mm valeurs dans le tableau egalement les bouttons d'accés

validateForm = function (f) {
    formData = f;
   
    if (document.getElementById('pdtId').value == "") {
        let line = document.getElementsByTagName('tbody')[0].insertRow();
        addCellInRow(line, ++pdtId);
        addCellInRow(line, f.article.value);
        addCellInRow(line, f.nature.value);
        addCellInRow(line, f.qté_ini.value);
        addCellInRow(line, f.qté_res.value);
        addCellInRow(line, f.prix.value);
        addCellInRow(line, f.localisation.value);
        addCellInRow(line, f.poids_vol.value);
        addCellButtonInRow(line, "Editer", pdtId, 'btn btn-primary', dataTarget = "edit");
        addCellButtonInRow(line, "Supprimer", pdtId, 'btn btn-danger', dataTarget = "delete");
        data.push({
            "pdtId": pdtId,
            "article": f.article.value,
            "nature": f.nature.value,
            "quantity_initiale": f.qté_ini.value,
            "quantity_restante": f.qté_res.value,
            "prix": f.prix.value,
            "localisation": f.localisation.value,
            "poids_vol": f.poids_vol.value
        });
    }


    clear(f);

}
